package de.verdion.mensaplan.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.MaskFilter;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.verdion.mensaplan.DataHolder.Config;
import de.verdion.mensaplan.DataHolder.MahlzeitObject;
import de.verdion.mensaplan.DataHolder.MensaBistroDataHolder;
import de.verdion.mensaplan.DataHolder.MensaPetrisbergDataHolder;
import de.verdion.mensaplan.DataHolder.MensaTarforstDataHolder;
import de.verdion.mensaplan.DataHolder.TagesTheken;
import de.verdion.mensaplan.DataHolder.ThekenObject;
import de.verdion.mensaplan.HelperClasses.EscapeUtils;
import de.verdion.mensaplan.HelperClasses.FileUtils;
import de.verdion.mensaplan.Logger.CLog;
import de.verdion.mensaplan.MainActivity;
import de.verdion.mensaplan.R;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Lucas on 24.03.2016.
 */
public class ListViewAdapter extends BaseAdapter implements StickyListHeadersAdapter{

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<MahlzeitObject> list;
    private Bitmap[] icons;
    private ArrayList<Integer> dateIndex;
    private ArrayList<String> dateList;
    private int priceId;
    private int location;

    public ListViewAdapter(Context context, int location){
        this.context = context;
        inflater = ((Activity) context).getLayoutInflater();
        dateIndex = new ArrayList<>();
        dateList = new ArrayList<>();
        this.location = location;
        initLists(location);
        priceId = Integer.parseInt(FileUtils.readPriceConfig(context));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_row2,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.extras = (TextView) convertView.findViewById(R.id.extras);
            viewHolder.location = (TextView) convertView.findViewById(R.id.location);
            viewHolder.price = (TextView) convertView.findViewById(R.id.price);
            viewHolder.icon = (CircularImageView) convertView.findViewById(R.id.icon);
            viewHolder.icon.setVisibility(View.VISIBLE);
            viewHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.loadingImage);
            convertView.setTag(viewHolder);
        } else {
          viewHolder = (ViewHolder) convertView.getTag();
        }

        MahlzeitObject obj = list.get(position);
        String title = EscapeUtils.unescape(obj.getLabel());
        if (title.length() > 55){
            viewHolder.title.setTextSize(13);
        } else {
            viewHolder.title.setTextSize(15);
        }
        viewHolder.title.setText(title);
        viewHolder.location.setText(obj.getThekenLabel());

        if (obj.getPrice().length == 3){
            if (String.valueOf(obj.getPrice()[priceId]).length() == 3){
                viewHolder.price.setText(obj.getPrice()[priceId]+"0€");
            } else {
                viewHolder.price.setText(obj.getPrice()[priceId] + "€");
            }

        }


        final ViewHolder finalViewHolder = viewHolder;
        Picasso.with(context).load("https://studiwerk.de/eo/scale?s=mensa_detail&p=" + obj.getHauptspeise().get(0).getUrl()).into(viewHolder.icon, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                finalViewHolder.icon.setVisibility(View.VISIBLE);
                finalViewHolder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                CLog.p("ERROR IMAGE");
            }
        });

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderVewHolder holder;
        if (convertView == null){
            holder = new HeaderVewHolder();
            convertView = inflater.inflate(R.layout.listview_header,parent,false);
            holder.title = (TextView) convertView.findViewById(R.id.headerDate);
            convertView.setTag(holder);
        } else {
            holder = (HeaderVewHolder) convertView.getTag();
        }

        String date = dateList.get(dateIndex.get(position));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        Date displayDate = null;
        try {
            displayDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE', den' dd.MM");
        String newDate = formatter.format(displayDate);
        holder.title.setText(newDate);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return dateIndex.get(position);
    }

    public static class ViewHolder{
        TextView title,extras,location,price;
        CircularImageView icon;
        ProgressBar progressBar;
    }

    public static class HeaderVewHolder{
        TextView title;
    }

    public void initLists(int location){
        switch (location){
            case 0:
                list = MensaTarforstDataHolder.getInstance().getAllInOneList();
                dateList = MensaTarforstDataHolder.getInstance().getDateList();
                dateIndex = MensaTarforstDataHolder.getInstance().getDateIndex();
                break;
            case 1:
                list = MensaBistroDataHolder.getInstance().getAllInOneList();
                dateList = MensaBistroDataHolder.getInstance().getDateList();
                dateIndex = MensaBistroDataHolder.getInstance().getDateIndex();
                break;
            case 2:
                list = MensaPetrisbergDataHolder.getInstance().getAllInOneList();
                dateList = MensaPetrisbergDataHolder.getInstance().getDateList();
                dateIndex = MensaPetrisbergDataHolder.getInstance().getDateIndex();
                break;
        }
    }

}
