package de.verdion.mensaplan.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import de.verdion.mensaplan.DataHolder.Config;
import de.verdion.mensaplan.R;

/**
 * Created by Lucas on 13.07.2016.
 *
 */
public class ChoiceDialogAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private String[] choices = new String[9];

    public ChoiceDialogAdapter(Context context) {
        this.context = context;
        inflater = ((Activity) context).getLayoutInflater();
        fillChoices();
    }

    @Override
    public int getCount() {
        return Config.CHOICES.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.dialog_choice_listview_row,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.dialog_choice_checkbox);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.dialog_choice_checkbox,viewHolder.checkBox);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Config.CHOICES[position] = "1";
                } else {
                    Config.CHOICES[position] = "0";
                }
            }
        });

        viewHolder.checkBox.setTag(position);
        viewHolder.checkBox.setText(choices[position]);

        if (Config.CHOICES[position].equals("1")){
            viewHolder.checkBox.setChecked(true);
        } else {
            viewHolder.checkBox.setChecked(false);
        }



        return convertView;
    }

    public static class ViewHolder{
        CheckBox checkBox;
    }

    private void fillChoices(){
        choices[0] = context.getString(R.string.choice_beilagen);
        choices[1] = context.getString(R.string.choice_tagessuppe);
        choices[2] = context.getString(R.string.choice_untergeschoss);
        choices[3] = context.getString(R.string.choice_Theke_1);
        choices[4] = context.getString(R.string.choice_Theke_2);
        choices[5] = context.getString(R.string.choice_wok);
        choices[6] = context.getString(R.string.choice_kleine_karte_for_u);
        choices[7] = context.getString(R.string.choice_kleine_karte);
        choices[8] = context.getString(R.string.choice_abendmensa);

    }
}
