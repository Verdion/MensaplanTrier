package de.verdion.mensaplan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.codebutler.farebot.NfcOffFragment;
import com.codebutler.farebot.card.desfire.DesfireException;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Arrays;

import de.verdion.mensaplan.Adapter.ChoiceDialogAdapter;
import de.verdion.mensaplan.Adapter.SectionsPagerAdapter;
import de.verdion.mensaplan.DataHolder.Config;
import de.verdion.mensaplan.DataHolder.MensaBistroDataHolder;
import de.verdion.mensaplan.DataHolder.MensaPetrisbergDataHolder;
import de.verdion.mensaplan.DataHolder.MensaTarforstDataHolder;
import de.verdion.mensaplan.Fragments.MensaTarforstFragment;
import de.verdion.mensaplan.HelperClasses.FileUtils;
import de.verdion.mensaplan.HelperClasses.IdShare;
import de.verdion.mensaplan.Logger.CLog;
import de.verdion.mensaplan.Networkaction.AddNewUser;
import de.verdion.mensaplan.Networkaction.LoadRatings;
import de.verdion.mensaplan.cardreader.Readers;
import de.verdion.mensaplan.cardreader.ValueData;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Dialog dialog;
    private Dialog dialogChoice;
    private static MainActivity mainActivity;

    public static String TAG = "Test";
    public static final String ACTION_FULLSCREEN = "de.verdion.mensaapp";
    public static final String EXTRA_VALUE = "valueData";
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private String[][] mTechLists;
    private IntentFilter mIntentFilter;
    private boolean mResumed = false;
    private boolean isNfcworking = false;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public static ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity = this;
        NfcManager manager = (NfcManager) this.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null)
            isNfcworking = true;

        File choiceFile = new File(getFilesDir() + "/choices");
        if (choiceFile.exists()){
            String data = FileUtils.readFileFromStorage(this, "choices");
            Config.CHOICES = data.split(":");
        } else {
            String choiceData = "0:0:0:0:0:0:0:0:0";
            Config.CHOICES = choiceData.split(":");
            FileUtils.writeFileToStorage(this, choiceData, "choices");
        }

        File rating = new File(getFilesDir() + "/rating");
        if (!rating.exists()){
            FileUtils.writeFileToStorage(this,"0:0","rating");
        } else {
            String data = FileUtils.readFileFromStorage(this,"rating");
            String dataSplit[] = data.split(":");
            if (dataSplit[0].equals("0") && Integer.parseInt(dataSplit[1]) >= 10){
                showRatingDialog();
            } else if (dataSplit[0].equals("0")){
                int value = Integer.parseInt(dataSplit[1]) + 1;
                FileUtils.writeFileToStorage(this,"0:" + value, "rating");
            }
        }

        boolean showVersionDialog = false;
        File newVersionFile = new File(getFilesDir() + "/version");
        if (!newVersionFile.exists()){
            FileUtils.writeFileToStorage(this, "1", "version");
            showVersionDialog = true;
        } else {
            int version = Integer.parseInt(FileUtils.readFileFromStorage(this,"version"));
            if (version <= 1){
                showVersionDialog = true;
            }
        }

        if (showVersionDialog){
            showVersionDialog();
            if (!isNfcworking){
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.no_nfc_title))
                        .setMessage(getString(R.string.no_nfc_message))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        }


        File file = getBaseContext().getFileStreamPath("id");
        if (!file.exists()){
            AddNewUser user = new AddNewUser(this);
            user.execute();
            FileUtils.savePriceConfig(this, "0");
            showPriceDialog(true);
        } else {
            IdShare.getInstance().userid = FileUtils.readId(this);
            CLog.p("ID", IdShare.getInstance().userid);
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (isNfcworking)
            toolbar.setSubtitle(getString(R.string.value_idle));
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                IdShare.getInstance().pageId = mViewPager.getCurrentItem();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        if (isNfcworking){
            mAdapter = NfcAdapter.getDefaultAdapter(this);
            mIntentFilter = new IntentFilter("android.nfc.action.ADAPTER_STATE_CHANGED");

            mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                    getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

            IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
            mFilters = new IntentFilter[] { tech, };
            mTechLists = new String[][] { new String[] { IsoDep.class.getName(),
                    NfcA.class.getName() } };

            if (getIntent().getAction().equals(ACTION_FULLSCREEN)) {
                ValueData valueData = (ValueData) getIntent().getSerializableExtra(EXTRA_VALUE);
                Log.w(TAG,"restoring data for fullscreen");
                Log.d("Test", valueData.value + "(ACTION_FULLSCREEN");

            }
        } else {
            CLog.p("Kein NFC");
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_setting2) {
            ((TextView) new AlertDialog.Builder(this)
                    .setTitle("Lizenzen")
                    .setMessage(Html.fromHtml(getString(R.string.settings2_message) + "<br><br>Icons der Zusatzstoffe und Appicon:<div>Icons made by <a href=\"http://www.freepik.com\" title=\"Freepik\">Freepik</a> from <a href=\"http://www.flaticon.com\" title=\"Flaticon\">www.flaticon.com</a> is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\" target=\"_blank\">CC 3.0 BY</a></div>" +
                            "<br>Herzicon:<div>Icons made by <a href=\"http://www.flaticon.com/authors/google\" title=\"Google\">Google</a> from <a href=\"http://www.flaticon.com\" title=\"Flaticon\">www.flaticon.com</a> is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\" target=\"_blank\">CC 3.0 BY</a></div>" +
                            "<br>CircularImageView:<div>CircularImageView <a href=\"https://github.com/lopspower/CircularImageView\" title=\"GitHub\">(GitHub)</a> is licensed by <a href=\"http://www.apache.org/licenses/LICENSE-2.0\" title=\"Apache License 2.0.\" target=\"_blank\">Apache License 2.0.</a></div>" +
                            "<br>Picasso:<div>Picasso <a href=\"https://github.com/square/picasso\" title=\"GitHub\">(GitHub)</a> is licensed by <a href=\"http://www.apache.org/licenses/LICENSE-2.0\" title=\"Apache License 2.0.\" target=\"_blank\">Apache License 2.0.</a></div>" +
                            "<br>ListViewAnimations:<div>ListViewAnimations <a href=\"https://github.com/nhaarman/ListViewAnimations\" title=\"GitHub\">(GitHub)</a> is licensed by <a href=\"http://www.apache.org/licenses/LICENSE-2.0\" title=\"Apache License 2.0.\" target=\"_blank\">Apache License 2.0.</a></div>" +
                            "<br>StickyListHeaders:<div>StickyListHeaders <a href=\"https://github.com/emilsjolander/StickyListHeaders\" title=\"GitHub\">(GitHub)</a> is licensed by <a href=\"http://www.apache.org/licenses/LICENSE-2.0\" title=\"Apache License 2.0.\" target=\"_blank\">Apache License 2.0.</a></div>" +
                            "<br>Nine Old Androids:<div>Nine Old Androids <a href=\"https://github.com/JakeWharton/NineOldAndroids\" title=\"GitHub\">(GitHub)</a> is licensed by <a href=\"http://www.apache.org/licenses/LICENSE-2.0\" title=\"Apache License 2.0.\" target=\"_blank\">Apache License 2.0.</a></div>"))
                    .show()
                    .findViewById(android.R.id.message))
                    .setMovementMethod(LinkMovementMethod.getInstance());
            return true;
        } else if (id == R.id.action_studiwerk){
            ((TextView) new AlertDialog.Builder(this)
                    .setTitle("Bereitgestellte Daten")
                    .setMessage(Html.fromHtml(getString(R.string.settings1_message1) + "<br><br><a href=\"http://studiwerk.de/\">Studierendenwerk Trier</a>"))
                    .show()
                    .findViewById(android.R.id.message))
                    .setMovementMethod(LinkMovementMethod.getInstance());
            return true;
        } else if (id == R.id.action_price){
            showPriceDialog(false);
        } else if (id == R.id.action_more){
            Intent intent = new Intent(MainActivity.this, MoreActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_choice){
            showChoiceDialog();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.radioButton:
                FileUtils.savePriceConfig(getApplicationContext(),"0");
                break;
            case R.id.radioButton2:
                FileUtils.savePriceConfig(getApplicationContext(), "1");
                break;
            case R.id.radioButton3:
                FileUtils.savePriceConfig(getApplicationContext(), "2");
                break;
        }

        new CountDownTimer(500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                dialog.dismiss();
                finish();
                startActivity(getIntent());
            }
        }.start();
    }

    private void showPriceDialog(boolean firstStart){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_price);
        if (firstStart)
            dialog.setTitle("Preise anzeigen für");
        else
            dialog.setTitle("Preisanzeige ändern");

        int price = Integer.parseInt(FileUtils.readPriceConfig(this));

        RadioButton r1 = (RadioButton) dialog.findViewById(R.id.radioButton);
        RadioButton r2 = (RadioButton) dialog.findViewById(R.id.radioButton2);
        RadioButton r3 = (RadioButton) dialog.findViewById(R.id.radioButton3);
        r1.setOnClickListener(this);
        r2.setOnClickListener(this);
        r3.setOnClickListener(this);

        if (!firstStart){
            switch (price){
                case 0:
                    r1.setChecked(true);
                    break;
                case 1:
                    r2.setChecked(true);
                    break;
                case 2:
                    r3.setChecked(true);
                    break;
            }
        }

        dialog.show();
    }

    private void showChoiceDialog(){
        dialogChoice = new Dialog(this);
        dialogChoice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogChoice.setContentView(R.layout.dialog_choice);
        dialogChoice.setCancelable(true);

        ListView listView = (ListView) dialogChoice.findViewById(R.id.dialog_choice_listview);
        Button button = (Button) dialogChoice.findViewById(R.id.dialog_choice_button);

        ChoiceDialogAdapter adapter = new ChoiceDialogAdapter(this);
        listView.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = "";
                for (int i = 0; i < Config.CHOICES.length; i++) {
                    data = data + Config.CHOICES[i] + ":";
                }
                FileUtils.writeFileToStorage(MainActivity.this, data.substring(0, data.length() - 1), "choices");
                Config.RELOAD_TARFORST = true;
                Config.RELOAD_BISTROAB = true;
                Config.RELOAD_PETRISBERG = true;
                finish();
                startActivity(getIntent());
                dialogChoice.dismiss();
            }
        });

        dialogChoice.show();
    }

    private void showVersionDialog(){
        AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
        alerDialogBuilder.setTitle(getString(R.string.new_in_this_version_title))
                .setMessage(getString(R.string.new_in_this_version_content))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.writeFileToStorage(MainActivity.this,"2","version");
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alerDialogBuilder.create();
        alertDialog.show();
    }

    private void showRatingDialog(){
        AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
        alerDialogBuilder.setTitle(getString(R.string.rating_title))
                .setMessage(getString(R.string.rating_message))
                .setCancelable(false)
                .setNeutralButton("bewerten", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.writeFileToStorage(MainActivity.this,"1:0", "rating");
                        Uri uri = Uri.parse("market://details?id=" + MainActivity.this.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName())));
                        }
                    }
                })
                .setNegativeButton("Später", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.writeFileToStorage(MainActivity.this,"0:0", "rating");
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Nie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.writeFileToStorage(MainActivity.this, "1:0", "rating");
                    }
                });

        AlertDialog alertDialog = alerDialogBuilder.create();
        alertDialog.show();
    }

    /*private boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null;
    }*/


    @Override
    public void onBackPressed(){
        if (dialogChoice != null){
            dialogChoice.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isNfcworking){
            getApplicationContext().registerReceiver(mReceiver, mIntentFilter);
            updateNfcState();
            mAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters,
                    mTechLists);
        }

        new LoadRatings().execute();
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (isNfcworking){
            Log.i(TAG, "Foreground dispatch");
            if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())) {
                Log.i(TAG,"Discovered tag with intent: " + intent);
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);


                try {
                    ValueData val = Readers.getInstance().readTag(tag);
                    Log.w(TAG,"Setting read data");

                } catch (DesfireException e) {
                    Toast.makeText(this,"Communication failed",Toast.LENGTH_SHORT).show();
                }
            } else if (getIntent().getAction().equals(ACTION_FULLSCREEN)) {
                ValueData valueData = (ValueData) getIntent().getSerializableExtra(EXTRA_VALUE);
                Log.d("Test", valueData+"");

            }
        }

    }

    public void updateNfcState() {

        if (!mAdapter.isEnabled() && mResumed) {
            NfcOffFragment f = new NfcOffFragment();
            f.show(getSupportFragmentManager(), NfcOffFragment.TAG);
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (isNfcworking){
                if (NfcAdapter.ACTION_ADAPTER_STATE_CHANGED.equals(action)) {
                    updateNfcState();
                }
            }
        }
    };

    public static void showValue(String value){
        MensaTarforstFragment fragment = (MensaTarforstFragment) mainActivity.getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
        fragment.showCardValue(value);
    }
}


