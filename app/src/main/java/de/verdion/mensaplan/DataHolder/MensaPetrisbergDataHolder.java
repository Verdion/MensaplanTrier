package de.verdion.mensaplan.DataHolder;

import java.util.ArrayList;

import de.verdion.mensaplan.HelperClasses.FoodUtils;
import de.verdion.mensaplan.Logger.CLog;

/**
 * Created by Lucas on 27.03.2016.
 *
 */
public class MensaPetrisbergDataHolder {
    private static MensaPetrisbergDataHolder instance;
    private ArrayList<TagesTheken> tagesTheken;
    private ArrayList<MahlzeitObject> allInOneList;
    private ArrayList<Integer> dateIndex;
    private ArrayList<String> dateList;

    public static MensaPetrisbergDataHolder getInstance() {
        if (MensaPetrisbergDataHolder.instance ==  null){
            MensaPetrisbergDataHolder.instance = new MensaPetrisbergDataHolder();
        }
        return MensaPetrisbergDataHolder.instance;
    }

    private MensaPetrisbergDataHolder() {
        tagesTheken = new ArrayList<>();
    }

    public void setTagesTheken(ArrayList<TagesTheken> tagesTheken) {
        this.tagesTheken = tagesTheken;
    }

    public ArrayList<TagesTheken> getTagesTheken() {
        return tagesTheken;
    }

    public void addTheke(TagesTheken obj){
        tagesTheken.add(obj);
    }

    public ArrayList<MahlzeitObject> getAllInOneList() {
        if (allInOneList == null || Config.RELOAD_PETRISBERG){
            allInOneList = new ArrayList<>();
            dateList = new ArrayList<>();
            dateIndex = new ArrayList<>();
            int counter = 0;
            for (TagesTheken tagestheke: tagesTheken) {
                for (ThekenObject theke: tagestheke.getThekenList()) {
                    String thekenLabel = theke.getLabel();
                    for (MahlzeitObject mahlzeit: theke.getMahlzeitList()) {
                        mahlzeit.setThekenLabel(thekenLabel);
                        if (!FoodUtils.isDisable(mahlzeit.getThekenLabel())){
                            if (!FoodUtils.isDisable(mahlzeit.getLabel())){
                                allInOneList.add(mahlzeit);
                                dateIndex.add(counter);
                            }
                        }
                    }
                    if (!dateList.contains(theke.getDate()+"")){
                        dateList.add(theke.getDate()+"");
                    }
                }
                counter++;
            }
        }

        Config.RELOAD_PETRISBERG = false;
        return allInOneList;
    }

    public ArrayList<Integer> getDateIndex() {
        if (dateIndex == null)
            getAllInOneList();
        return dateIndex;
    }

    public ArrayList<String> getDateList() {
        if (dateList == null)
            getAllInOneList();
        return dateList;
    }

    public void setAllInOneList(ArrayList<MahlzeitObject> allInOneList) {
        this.allInOneList = allInOneList;
    }

}
