package de.verdion.mensaplan.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

import de.verdion.mensaplan.Adapter.ListViewAdapterBeilagen;
import de.verdion.mensaplan.Adapter.ListViewAdapterZusatz;
import de.verdion.mensaplan.DataHolder.EssenObject;
import de.verdion.mensaplan.DataHolder.MahlzeitObject;
import de.verdion.mensaplan.DataHolder.MensaBistroDataHolder;
import de.verdion.mensaplan.DataHolder.MensaPetrisbergDataHolder;
import de.verdion.mensaplan.DataHolder.MensaTarforstDataHolder;
import de.verdion.mensaplan.DataHolder.RatingDataHolder;
import de.verdion.mensaplan.DataHolder.ZusatzObject;
import de.verdion.mensaplan.DetailSwipeActivity;
import de.verdion.mensaplan.HelperClasses.BitmapShare;
import de.verdion.mensaplan.HelperClasses.EscapeUtils;
import de.verdion.mensaplan.HelperClasses.IdShare;
import de.verdion.mensaplan.Logger.CLog;
import de.verdion.mensaplan.Networkaction.Like;
import de.verdion.mensaplan.Networkaction.Unlike;
import de.verdion.mensaplan.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private ImageButton icon;
    private TextView title;
    private ListView zusatzListView, beilagenListView;
    private Bundle bundle;
    private ListViewAdapterZusatz adapter;
    private ListViewAdapterBeilagen adapterBeilagen;
    private RelativeLayout beilagenLayout, zusatzLayout;
    private Context context;
    private ArrayList<ZusatzObject> zusatzList;
    private ImageButton ratinIcon;
    private TextView ratingTextview;
    private Animation animation;
    private int clickIndex = 0;
    private ArrayList<MahlzeitObject> list;
    private int id,today,all;
    private boolean liked;

    public DetailFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(int position) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        list = getAllinOneList(IdShare.getInstance().pageId);

        icon = (ImageButton) v.findViewById(R.id.detailIcon2SwipeDetail);
        //icon.setImageDrawable(BitmapShare.getInstance().bitmapDrawable);
        Picasso.with(context).load("https://studiwerk.de/eo/scale?s=mensa_detail&p=" + list.get(getArguments().getInt("position")).getHauptspeise().get(0).getUrl()).into(icon);
        title = (TextView) v.findViewById(R.id.detailTitleSwipeDetail);
        zusatzListView = (ListView) v.findViewById(R.id.listViewZusatzSwipeDetail);
        beilagenListView = (ListView) v.findViewById(R.id.listViewBeilagenSwipeDetail);
        beilagenLayout = (RelativeLayout) v.findViewById(R.id.layoutBeilagenSwipeDetail);
        zusatzLayout = (RelativeLayout) v.findViewById(R.id.layoutZusatzSwipeDetail);
        ratinIcon = (ImageButton) v.findViewById(R.id.ratingIconSwipeDetail);
        ratingTextview = (TextView) v.findViewById(R.id.ratingTextviewSwipeDetail);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.like_animation);

        ratinIcon.setOnClickListener(this);
        icon.setOnClickListener(this);

        zusatzListView.setDivider(null);
        beilagenListView.setDivider(null);
        context = getActivity();
        zusatzList = generateZusatzList(IdShare.getInstance().pageId, getArguments().getInt("position"));
        setLayoutWeight();

        title.setText(EscapeUtils.unescape(list.get(getArguments().getInt("position")).getLabel()));
        setRatingText(false);
        initListViews();


        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ratingIconSwipeDetail:
                if (!liked){
                    like();
                } else {
                    unlike();
                }

                break;
            case R.id.detailIcon2SwipeDetail:
                if (!liked)
                    detectDoubleClick();
                break;
        }
    }

    private void detectDoubleClick(){
        clickIndex++;
        Handler handler = new Handler();
        Runnable r = new Runnable() {

            @Override
            public void run() {
                clickIndex = 0;
            }
        };

        if (clickIndex == 1) {
            //Single click
            handler.postDelayed(r, 250);
        } else if (clickIndex == 2) {
            //Double click
            clickIndex = 0;
            like();
        }
    }

    private void like(){
        ratinIcon.setImageResource(R.mipmap.ic_favorite_black_24dp);
        ratinIcon.startAnimation(animation);
        setRatingText(true);
        Like like = new Like();
        like.execute(list.get(getArguments().getInt("position")).getId());
    }

    private void initListViews(){
        adapterBeilagen = new ListViewAdapterBeilagen(context,list, getArguments().getInt("position"));
        adapter = new ListViewAdapterZusatz(context,zusatzList);
        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
        ScaleInAnimationAdapter animationAdapter2 = new ScaleInAnimationAdapter(adapterBeilagen);
        animationAdapter2.setAbsListView(beilagenListView);
        animationAdapter.setAbsListView(zusatzListView);
        zusatzListView.setAdapter(animationAdapter);
        beilagenListView.setAdapter(animationAdapter2);
    }

    private void setLayoutWeight(){
        float weight = 0.05f;
        switch (zusatzList.size()){
            case 1:
                weight+=0.06;
                break;
            case 2:
                weight+=0.15;
                break;
            case 3:
                weight+=0.26;
                break;
            case 4:
                weight+=0.35;
                break;
            case 5:
                weight+=0.35;
                break;
            case 6:
                weight+=0.35;
                break;
            case 7:
                weight+=0.35;
                break;
        }

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 0);
        params1.weight = weight;
        params2.weight = 1-weight;

        zusatzLayout.setLayoutParams(params1);
        beilagenLayout.setLayoutParams(params2);

    }

    private ArrayList<ZusatzObject> generateZusatzList(int location,int position){

        EssenObject obj = list.get(position).getHauptspeise().get(0);
        ArrayList<ZusatzObject> zusatz = new ArrayList<>();
        if (obj.isRind()){
            zusatz.add(new ZusatzObject("enthält Rindfleisch", R.mipmap.beef));
        }
        if (obj.isSchwein()){
            zusatz.add(new ZusatzObject("enthält Schweinefleisch", R.mipmap.pig));
        }
        if (obj.isHuhn()){
            zusatz.add(new ZusatzObject("enthält Geflügelfleisch", R.mipmap.chicken));
        }
        if (obj.isFisch()){
            zusatz.add(new ZusatzObject("enthält Fisch", R.mipmap.fish));
        }
        if (obj.isVegetarisch()){
            zusatz.add(new ZusatzObject("Vegetarisch", R.mipmap.veggie));
        }
        if (obj.isVegan()){
            zusatz.add(new ZusatzObject("Vegan", R.mipmap.vegan));
        }
        if (obj.isLaktosefrei()){
            zusatz.add(new ZusatzObject("Laktosefrei", R.mipmap.lactose));
        }

        return zusatz;
    }

    private ArrayList<MahlzeitObject> getAllinOneList(int location){
        ArrayList<MahlzeitObject> list = null;
        switch (location){
            case 0:
                list = MensaTarforstDataHolder.getInstance().getAllInOneList();
                break;
            case 1:
                list = MensaBistroDataHolder.getInstance().getAllInOneList();
                break;
            case 2:
                list = MensaPetrisbergDataHolder.getInstance().getAllInOneList();
                break;
        }

        return list;
    }

    private void setRatingText(boolean like){
        if (like){
            if (all == -1){
                all += 2;
            } else {
                all++;
            }
            today++;
            RatingDataHolder.getInstance().addLike(id);
            liked = true;
        } else {
            id = list.get(getArguments().getInt("position")).getId();
            today = RatingDataHolder.getInstance().getRatingDayForId(id);
            all = RatingDataHolder.getInstance().getRatingForId(id);
        }

        String suffix;
        if (today == 1){
            suffix = " Person";
        } else {
            suffix = " Personen";
        }

        if (all == -1){
            ratingTextview.setText("Noch keine Bewertung");
        } else {
            ratingTextview.setText("Schmeckt heute " + today + suffix + "\n" + all + " gesamt");
        }

        if (RatingDataHolder.getInstance().hasUserRating(id)){
            ratinIcon.setImageResource(R.mipmap.ic_favorite_black_24dp);
            liked = true;
        }
    }

    private void unlike(){
        CLog.p("UNLIKE");
        today--;
        all--;
        RatingDataHolder.getInstance().removeLike(id);
        ratinIcon.setImageResource(R.mipmap.ic_favorite_border_black_24dp);
        setRatingText(false);
        liked = false;
        Unlike unlike = new Unlike();
        unlike.execute(id);
    }

}
