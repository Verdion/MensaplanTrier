package de.verdion.mensaplan.Fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.nhaarman.listviewanimations.appearance.StickyListHeadersAdapterDecorator;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import de.verdion.mensaplan.DataHolder.Config;
import de.verdion.mensaplan.DataHolder.MensaBistroDataHolder;
import de.verdion.mensaplan.DataHolder.MensaPetrisbergDataHolder;
import de.verdion.mensaplan.DataHolder.TagesTheken;
import de.verdion.mensaplan.DetailActivity;
import de.verdion.mensaplan.DetailSwipeActivity;
import de.verdion.mensaplan.HelperClasses.BitmapShare;
import de.verdion.mensaplan.HelperClasses.IdShare;
import de.verdion.mensaplan.MainActivity;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import de.verdion.mensaplan.Adapter.ListViewAdapter;
import de.verdion.mensaplan.DataHolder.MensaTarforstDataHolder;
import de.verdion.mensaplan.Logger.CLog;
import de.verdion.mensaplan.Networkaction.LoadMensaData;
import de.verdion.mensaplan.R;

/**
 * Created by Lucas on 24.03.2016.
 */
public class MensaTarforstFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String ARG_SECTION_NUMBER = "location";
    private StickyListHeadersListView listView;
    private ProgressBar progressBar;
    private int location;
    public ListViewAdapter adapter;
    private RelativeLayout valueLayout;
    private TextView valueTextview;
    private Animation animationUp,animationDown;
    private int firstHeight = -1;


    public MensaTarforstFragment() {
    }


    public static MensaTarforstFragment newInstance(int sectionNumber) {
        MensaTarforstFragment fragment = new MensaTarforstFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (StickyListHeadersListView) rootView.findViewById(R.id.listView);
        valueLayout = (RelativeLayout) rootView.findViewById(R.id.priceValueRelative);
        valueTextview = (TextView) rootView.findViewById(R.id.priceValue);
        animationUp = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up);
        animationDown = AnimationUtils.loadAnimation(getContext(),R.anim.slide_down);
        listView.setDivider(null);
        listView.setOnItemClickListener(this);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBarMain);
        boolean loadData = false;
        location = this.getArguments().getInt(ARG_SECTION_NUMBER);
        ArrayList<TagesTheken> thekenList = null;


        switch (location){
            case 0:
                thekenList = MensaTarforstDataHolder.getInstance().getTagesTheken();
                if (MensaTarforstDataHolder.getInstance().getTagesTheken().size() == 0)
                    loadData = true;
                break;
            case 1:
                thekenList = MensaBistroDataHolder.getInstance().getTagesTheken();
                if (MensaBistroDataHolder.getInstance().getTagesTheken().size() == 0)
                    loadData = true;
                break;
            case 2:
                thekenList = MensaPetrisbergDataHolder.getInstance().getTagesTheken();
                if (MensaPetrisbergDataHolder.getInstance().getTagesTheken().size() == 0)
                    loadData = true;
                break;
        }


        if (loadData){
            LoadMensaData loadMensaData = new LoadMensaData(getActivity(),listView,progressBar,location);
            loadMensaData.execute(this.getArguments().getInt(ARG_SECTION_NUMBER)+"");
        } else {
            adapter = new ListViewAdapter(getActivity(),location);
            ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
            StickyListHeadersAdapterDecorator stickyListHeadersAdapterDecorator = new StickyListHeadersAdapterDecorator(animationAdapter);
            stickyListHeadersAdapterDecorator.setStickyListHeadersListView(listView);
            listView.setAdapter(stickyListHeadersAdapterDecorator);
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            CLog.p("Loading Data from memory");
        }


        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CircularImageView icon = (CircularImageView) view.findViewById(R.id.icon);
        TextView title = (TextView) view.findViewById(R.id.title);
        BitmapShare.getInstance().bitmapDrawable = (BitmapDrawable) icon.getDrawable();

        Pair<View,String> p1 = Pair.create((View) icon, "icon");
        Pair<View,String> p2 = Pair.create((View) title, "title");

        Bundle bundle = new Bundle();
        bundle.putString("title", title.getText().toString());
        bundle.putInt("titleSize", title.getWidth());
        bundle.putInt("position", position);
        bundle.putInt("location", location);

        Intent intent = new Intent(getActivity(), DetailSwipeActivity.class);
        intent.putExtras(bundle);
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),p1,p2);
        startActivity(intent,optionsCompat.toBundle());
    }

    public void showCardValue(final String value){

        valueLayout.setVisibility(View.VISIBLE);
        Resources r = getResources();
        int px = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, r.getDisplayMetrics()));

        if (firstHeight == -1){
            firstHeight = valueLayout.getMeasuredHeight() + px;
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) valueLayout.getLayoutParams();
        params.height = firstHeight;
        valueLayout.setLayoutParams(params);



        valueLayout.startAnimation(animationUp);
        valueTextview.setText(value);

        new CountDownTimer(3500,3500){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                valueLayout.startAnimation(animationDown);
                valueLayout.setVisibility(View.GONE);
                valueLayout.setVisibility(View.INVISIBLE);
            }
        }.start();
    }
}
