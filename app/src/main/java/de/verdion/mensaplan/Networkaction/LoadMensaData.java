package de.verdion.mensaplan.Networkaction;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.StickyListHeadersAdapterDecorator;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.verdion.mensaplan.Adapter.ListViewAdapter;
import de.verdion.mensaplan.DataHolder.DataParser;
import de.verdion.mensaplan.DataHolder.MensaBistroDataHolder;
import de.verdion.mensaplan.DataHolder.MensaPetrisbergDataHolder;
import de.verdion.mensaplan.DataHolder.MensaTarforstDataHolder;
import de.verdion.mensaplan.DataHolder.TagesTheken;
import de.verdion.mensaplan.DataHolder.ThekenObject;
import de.verdion.mensaplan.HelperClasses.IdShare;
import de.verdion.mensaplan.Logger.CLog;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Lucas on 22.03.2016.
 */
public class LoadMensaData extends AsyncTask<String,Void,Void> {
    private StringBuilder builder,builder2;
    private Context context;
    private StickyListHeadersListView listView;
    private ProgressBar progressBar;
    private boolean error = false;
    private int location;

    public LoadMensaData(Context context, StickyListHeadersListView listView, ProgressBar progressBar,int location){
        this.context = context;
        this.listView = listView;
        this.progressBar = progressBar;
        this.location = location;
    }

    @Override
    protected Void doInBackground(String... params) {

        location = Integer.parseInt(params[0]);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        builder = new StringBuilder();
        builder2 = new StringBuilder();
        CLog.p("Loading Data from Server");

        try {
            URL url = new URL("url");
            URL url2 = new URL("url");
            CLog.p("PROTOKOL", url.getProtocol());
            BufferedReader buf = new BufferedReader(new InputStreamReader(url.openStream()));
            BufferedReader buf2 = new BufferedReader(new InputStreamReader(url2.openStream()));
            String line;
            String line2;

            while((line = buf.readLine()) != null){
                builder.append(line);
            }

            while((line2 = buf2.readLine()) != null){
                builder2.append(line2);
            }

            buf.close();
            buf2.close();
            DataParser.parseRating(builder2.toString());
            DataParser.parse(builder.toString(), Integer.parseInt(params[0]));
        } catch (IOException e) {
            e.printStackTrace();
            error = true;
            CLog.p("Exception LoadMensaData", e.toString(),null);
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!error){
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            ListViewAdapter adapter = new ListViewAdapter(context,location);
            ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
            StickyListHeadersAdapterDecorator stickyListHeadersAdapterDecorator = new StickyListHeadersAdapterDecorator(animationAdapter);
            stickyListHeadersAdapterDecorator.setStickyListHeadersListView(listView);
            listView.setAdapter(stickyListHeadersAdapterDecorator);
        } else {
            Toast.makeText(context, "Ein Fehler ist aufgetreten. Prüfe deine Internetverbindung", Toast.LENGTH_LONG).show();
        }

        return;
    }
}
