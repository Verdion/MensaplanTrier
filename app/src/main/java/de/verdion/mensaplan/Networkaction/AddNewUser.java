package de.verdion.mensaplan.Networkaction;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;

import de.verdion.mensaplan.DataHolder.DataParser;
import de.verdion.mensaplan.HelperClasses.FileUtils;
import de.verdion.mensaplan.HelperClasses.IdShare;
import de.verdion.mensaplan.Logger.CLog;

/**
 * Created by Lucas on 31.03.2016.
 */
public class AddNewUser extends AsyncTask<String,Void,Void> {

    private StringBuilder builder;
    private Context context;
    private boolean error;

    public AddNewUser(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... params) {
        builder = new StringBuilder();
        try {
            URL url = new URL("url");
            BufferedReader buf = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;

            while((line = buf.readLine()) != null){
                builder.append(line);
            }
            buf.close();
            if (builder.toString().equals("Fehlercode 1")){
                error = true;
            } else {
                FileUtils.saveId(context,builder.toString());
                IdShare.getInstance().userid = builder.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
            CLog.p("Exception AddNewUser", e.toString(), null);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (error){
            AddNewUser user = new AddNewUser(context);
            user.execute();
        }

    }
}
