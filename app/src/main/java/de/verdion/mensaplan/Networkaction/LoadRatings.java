package de.verdion.mensaplan.Networkaction;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.verdion.mensaplan.DataHolder.DataParser;
import de.verdion.mensaplan.HelperClasses.IdShare;

/**
 * Created by Lucas on 06.04.2016.
 */
public class LoadRatings extends AsyncTask<String,Void,Void> {

    private StringBuilder builder;

    @Override
    protected Void doInBackground(String... params) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        builder = new StringBuilder();

        try {
            URL url = new URL("url");
            BufferedReader buf = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;

            while((line = buf.readLine()) != null){
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataParser.parseRating(builder.toString());

        return null;
    }
}
