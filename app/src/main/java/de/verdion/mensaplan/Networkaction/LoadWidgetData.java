package de.verdion.mensaplan.Networkaction;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.verdion.mensaplan.DataHolder.DataParser;
import de.verdion.mensaplan.DataHolder.DataParserWidget;
import de.verdion.mensaplan.DataHolder.TagesTheken;
import de.verdion.mensaplan.Logger.CLog;

/**
 * Created by Lucas on 04.04.2016.
 */
public class LoadWidgetData extends AsyncTask<Integer, Void, String> {

    private int location;
    private StringBuilder builder;


    @Override
    protected String doInBackground(Integer... params) {
        location = params[0];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        builder = new StringBuilder();
        CLog.p("Loading Data from Server");

        try {
            URL url = new URL("url");
            BufferedReader buf = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;

            while((line = buf.readLine()) != null){
                builder.append(line);
            }

            buf.close();
            //DataParserWidget.parse(builder.toString());
        } catch (IOException e) {
            e.printStackTrace();
            CLog.p("Exception LoadMensaData", e.toString(),null);
        }

        return builder.toString();
    }

}
