package de.verdion.mensaplan.Networkaction;

import android.appwidget.AppWidgetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import de.verdion.mensaplan.Logger.CLog;
import de.verdion.mensaplan.R;
import de.verdion.mensaplan.Widget.RemoteViewsService;
import de.verdion.mensaplan.Widget.WidgetProvider;

/**
 * Created by Lucas on 04.04.2016.
 */
public class LoadImageForWidget extends AsyncTask<String,Void,Bitmap> {

    private Bitmap bitmap;
    private RemoteViews view;
    private int WidgetID;
    private AppWidgetManager WidgetManager;




    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            URL imageURL = new URL("url");
            bitmap = BitmapFactory.decodeStream(imageURL.openStream());
        } catch (IOException e) {
            bitmap = null;
            CLog.p("ERROR", e.toString());
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
    }
}
