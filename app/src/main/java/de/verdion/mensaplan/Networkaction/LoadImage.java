package de.verdion.mensaplan.Networkaction;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.verdion.mensaplan.Adapter.ListViewAdapter;

/**
 * Created by Lucas on 26.03.2016.
 */
public class LoadImage extends AsyncTask<String,Void,Void> {

    private Bitmap bitmap;
    private ListViewAdapter.ViewHolder viewHolder;


    public LoadImage(ListViewAdapter.ViewHolder viewHolder){
        this.viewHolder = viewHolder;
    }


    @Override
    protected Void doInBackground(String... params) {
        downloadImage(params[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //progressBar.setVisibility(View.GONE);
        //icon.setImageBitmap(bitmap);
        //icon.setVisibility(View.VISIBLE);
        //adapter.setBitmap(bitmap,position);

    }

    private Bitmap downloadImage(String urlString){
        try {
            URL url = new URL("url");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            bitmap = scaleImage(BitmapFactory.decodeStream(inputStream));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Bitmap scaleImage(Bitmap bitmap){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float scaleWidth = ((float) 200) / width;
        float scaleHeight = ((float) 200) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth,scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap,0,0,width,height,matrix,false);
        return resizedBitmap;
    }
}
